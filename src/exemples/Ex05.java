package exemples;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Ex05 {

    public static void main(String[] args) throws  Exception {
        
        Connection cxBdd =    connexionBdd() ;
      
        String requete= "Select nom, prenom , sexe, poids  " +
                        "From   Personne                   " +
                        "Where  poids =(select max(poids) from Personne)" 
                        ;                           
                         
        Statement   cmde   = cxBdd.createStatement();
        ResultSet   pers   = cmde.executeQuery(requete);
   
        System.out.println("\n\nLe(s) Judoka(s) le(s) plus lourds est(sont):\n ");
        
        while(  pers.next() ) {
            
          afficher(pers);
        }
        
        System.out.println("\n");
        pers.close();
        cxBdd.close();
    }

    
     private static void afficher(ResultSet pers) throws SQLException {
        
        final String format=" %-15s %-15s %-2s %4d kg\n";
        System.out.printf(format,
                                      
                          pers.getString("nom"),
                          pers.getString("prenom"),
                          pers.getString("sexe"),
                          pers.getInt   ("poids")
                         );
    }

    
   private static Connection connexionBdd() throws Exception
    {
        
        Connection connexion=null;
        Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        connexion =  DriverManager.getConnection("jdbc:derby://localhost:1527/BddFederationJudo","uJudo","dojo") ;
        return connexion;
    }
}