package exemples;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class Ex04 {

    public static void main(String[] args) throws Exception {
        
        Connection cxBdd =    connexionBdd() ;
      
        String requete= "Select max(poids) as poidsMax " +
                        "From   Personne               " ;                           
                        
        
        Statement   cmde   = cxBdd.createStatement();
        ResultSet   pers   = cmde.executeQuery(requete);
   
         
        if(  pers.next() ) {
            
           System.out.printf("\n Le poids le plus élevé: %4d kg\n",
  
                              pers.getInt("poidsMax")
                            );
        }
        
        System.out.println("\n");
        pers.close();
        cxBdd.close();
    }

     private static Connection connexionBdd() throws Exception
    {
        
        Connection connexion=null;
        Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        connexion =  DriverManager.getConnection("jdbc:derby://localhost:1527/BddFederationJudo","uJudo","dojo") ;
        return connexion;
    } 
}