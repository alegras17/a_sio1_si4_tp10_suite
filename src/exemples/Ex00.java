package exemples;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import utilitaires.UtilDate;

public class Ex00 {

    public static void main(String[] args) throws Exception {
        
        Connection cxBdd =    connexionBdd() ;
      
        String requete= "Select nom, prenom, sexe, datenaiss, poids, ville, nbvictoires " +
                        "From   Personne "    +
                        "Order  by nom, prenom";
        
        Statement   cmde   = cxBdd.createStatement();
        ResultSet   pers   = cmde.executeQuery(requete);
   
        System.out.println("\nLes Membres du Club:\n\n");
         
        while (  pers.next() ) {
            
            afficher(pers);
         }
        
        System.out.println("\n");
        pers.close();
        cxBdd.close();
    }

    private static void afficher(ResultSet pers) throws SQLException {
        
        final String format=" %-15s %-15s %-2s %-10s %4d kg %-15s %2d victoires\n";
        System.out.printf(format,
                                      pers.getString(1),
                                      pers.getString(2),
                                      pers.getString(3),
                                      UtilDate.dateVersChaine(pers.getDate(4)),
                                      pers.getInt(5),
                                      pers.getString(6),
                                      pers.getInt(7));
    }

    private static Connection connexionBdd() throws Exception
    {
        
        Connection connexion=null;
        Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        connexion =  DriverManager.getConnection("jdbc:derby://localhost:1527/BddFederationJudo","uJudo","dojo") ;
        return connexion;
    }  
}