package exemples;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import static utilitaires.UtilDate.*;

public class Ex01 {

    public static void main(String[] args) throws Exception {
        
        Connection cxBdd =    connexionBdd() ;
      
        String requete= "Select nom, prenom,datenaiss, poids, ville, nbvictoires " +
                        "From   Personne "                                         +
                        "Where  sexe='F' "                                         + 
                        "Order  by nom, prenom";
        
        Statement   cmde   = cxBdd.createStatement();
        ResultSet   pers   = cmde.executeQuery(requete);
   
        System.out.println("\nLes Membres du Club de sexe féminin :\n\n");
         
        while (  pers.next() ) {
            
            afficher(pers);
         }
        
        System.out.println("\n");
        pers.close();
        cxBdd.close();
    }

    private static void afficher(ResultSet pers) throws SQLException {
        
        final String format=" %-15s %-15s %-10s %4d kg %-15s %2d victoires\n";
        System.out.printf(format,
                                      
                          pers.getString("nom"),
                          pers.getString("prenom"),
                          dateVersChaine(pers.getDate("datenaiss")),
                          pers.getInt("poids"),
                          pers.getString("ville"),
                          pers.getInt("nbVictoires"));
    }

     private static Connection connexionBdd() throws Exception
    {
        
        Connection connexion=null;
        Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        connexion =  DriverManager.getConnection("jdbc:derby://localhost:1527/BddFederationJudo","uJudo","dojo") ;
        return connexion;
    } 
}