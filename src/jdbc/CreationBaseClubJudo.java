package jdbc;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.Statement;

public class CreationBaseClubJudo {
    


    public static void main(String[] args) throws Exception {
                 
        Connection conn = connexionBdd();
        
        DriverManager.getConnection("jdbc:derby://localhost:1527/BddFederationJudo","uJudo","dojo") ;
        
        Statement  cmde=conn.createStatement();
       
        cmde.executeUpdate(  "Create table Personne"
                          +  "(" 
                          +  "   id          integer primary key, "
                          +  "   nom         varchar(30), "
                          +  "   prenom      varchar(30), "
                          +  "   sexe        varchar(1), "
                          +  "   datenaiss   date, "
                          +  "   poids       integer, "
                          +  "   ville       varchar(60), "
                          +  "   nbvictoires integer"
                          +  ")"
                          );
     
        cmde.executeUpdate( " insert into Personne values (1,'Durant','Pierre','M','05/12/1993',83,'Arras',9 ) " );
        cmde.executeUpdate( " insert into Personne values (2,'Martin','Sophie','F','11/25/1991',52,'Lens',6) " );  
        cmde.executeUpdate( " insert into Personne values (3,'Lecoutre','Thierry','M','08/05/1992',72,'Arras',5) " );  
         
        cmde.executeUpdate( " insert into Personne values (4,'Duchemin','Fabienne','F','03/14/1992',61,'Lens',10 ) " );  
        cmde.executeUpdate( " insert into Personne values (5,'Duchateau','Jacques','M','07/18/1992',91,'Bapaume',4 ) " );  
        cmde.executeUpdate( " insert into Personne values (6,'Lemortier','Laurent','M','02/18/1989',76,'Arras',12 ) " );  

        cmde.executeUpdate( " insert into Personne values (7,'Dessailles','Sabine','F','03/23/1990',68,'Arras',3 ) " );  
        cmde.executeUpdate( " insert into Personne values (8,'Bataille','Boris','M','11/17/1991',102,'Vitry-en-Artois',7) " );  
        cmde.executeUpdate( " insert into Personne values (9,'Lerouge','Laëtitia','F','10/09/1992',46,'Lens',4 ) " );  
         
        cmde.executeUpdate( " insert into Personne values (10,'Renard','Paul','M','08/16/1992',68,'Lens',9 ) " );  
        cmde.executeUpdate( " insert into Personne values (11,'Durant','Jacques','M','04/13/1990',75,'Arras',4 ) " );  
        cmde.executeUpdate( " insert into Personne values (12,'Delespaul','Martine','F','02/25/1991',55,'Lens',6 ) " );  
       
    }
    
    
    private static Connection connexionBdd() throws Exception
    {
        
        Connection connexion=null;
        Class.forName("org.apache.derby.jdbc.ClientDriver").newInstance();
        connexion =  DriverManager.getConnection("jdbc:derby://localhost:1527/BddFederationJudo","uJudo","dojo") ;
        return connexion;
    }
    
}
